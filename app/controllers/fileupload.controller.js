const Product = require('../models/product.model.js');
var formidable = require('formidable');
var fs = require('fs');
var mv = require('mv');
const csv = require('fast-csv');
const csvParser = require('csv-parser');
var resolve = require('path').resolve;
const { ObjectId } = require('mongodb');

function stringConverter(string, option) {
    var convertedArray = [];
    var data;
    if (string) {
        data = string.split(',');
        var i = 0;
        if (option === 'features') {
            for (; i < data.length; i++) {
                var pairObj = {};
                var pairs = data[i].split(':');
                pairObj['key'] = pairs[0];
                pairObj['val'] = pairs[1];
                convertedArray.push(pairObj);

            }
        } else if (option === 'categories')
            for (; i < data.length; i++) {
                convertedArray.push({'_id': data[i]});
            }
    }
    return convertedArray;
}

// Create and Save a new Note
exports.create = (req, res) => {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        var oldpath = files.filetoupload.path;
        var newpath = resolve('./app/files/' + files.filetoupload.name);
        console.log(newpath);
        mv(oldpath, newpath, async function (err) {
            if (err) throw err;
            res.write('File uploaded and moved!');
            var stream = fs.readFileSync(newpath, 'utf8');
            const products = [];
            const payload = [];
            var productObj = {};
            let success = [], errors = [];

            var csvStream = csv.fromString(stream, {
                headers: true,
                ignoreEmpty: true
            }).validate(function (data, next) {
                Product.findById(data.product_id, function (err, model) {
                    if (err) {
                        next(err);
                    } else {
                        next(null, model);
                    }
                });
            })
                .on("data", function (data) {
                    // console.log('processing...');
                    products.push(data)
                })
                .on("end", async function () {
                    for (var i = 0; i < products.length; ) {
                        try {
                        let wheatherUpdateImg = false;
                        var temp = i + 1;
                        var variants = [];
                        var variantsObj = {};
                        var images = [];
                        var categories = [];
                        if (products[i].imageUrls) {
                            images = products[i].imageUrls.split(',');
                        }
                        if (products[i].featured_image) {
                            images.unshift(products[i].featured_image);
                        }
                        if (products[i].categories) {
                            categories = products[i].categories.split(',');
                        }
                        productObj['_id'] = products[i].product_id;
                        productObj['name'] = products[i].name;
                        productObj['sku'] = products[i].sku;
                        productObj['brand'] = products[i].brand;
                        productObj['categories'] = stringConverter(products[i].categories, 'categories');
                        productObj['imageUrls'] = images;
                        productObj['features'] = stringConverter(products[i].features, 'features');
                        productObj['vendor_id'] = products[i].vendor_name;
                        productObj['vendor_name'] = products[i].vendor_name;
                        productObj['vendor_email'] = products[i].vendor_email;
                        productObj['description'] = products[i].description;
                        productObj['metaTitle'] = products[i].metaTitle;
                        productObj['metaDescription'] = products[i].metaDescription;
                        productObj['metaKeywords'] = products[i].metaKeywords;
                        productObj['brandName'] = products[i].brandName;
                        productObj['sizeChartUrl'] = products[i].sizechart;
                        productObj['new'] = products[i].new;
                        productObj['sale'] = products[i].sale;
                        productObj['hot'] = products[i].hot;
                        productObj['approved'] = products[i].approved;
                        productObj['active'] = products[i].active;
                        productObj['popularity'] = products[i].popularity;
                        productObj['featured'] = products[i].featured;
                        productObj['enableZips'] = products[i].enableZips;
                        productObj['featured'] = products[i].featured;
                        // (products[temp]) ? console.log('temp') : console.log('nottemp');
                        var sku = '';
                        if (products[temp]) {
                            sku = products[temp].sku;
                        }
                        variants = [];
                        // console.log(products.length);
                        if (products[i].sku === sku) {
                            var count = i;
                            var checkSku = products[i].sku;
                            while (checkSku === sku) {
                                variantsObj = {};
                                variantsObj['_id'] = products[i].variant_id;
                                variantsObj['size'] = products[i].size;
                                variantsObj['sku'] = products[i].variant_sku;
                                variantsObj['active'] = products[i].active;
                                variantsObj['stock'] = products[i].stock;
                                variantsObj['shipping'] = products[i].shipping;
                                variantsObj['mrp'] = products[i].mrp;
                                variantsObj['price'] = products[i].price;
                                // variantsObj['sort'] = temp+1;
                                variants.push(variantsObj);
                                count++;
                                if (products[count]) {
                                    sku = products[count].sku;
                                } else {
                                    sku = '';
                                    checkSku = products[i].sku;
                                }
                                i++;

                            }
                            // console.log(variants);
                            productObj['variants'] = variants;
                        } else {
                            variantsObj['_id'] = products[i].variant_id;
                            variantsObj['size'] = products[i].size;
                            variantsObj['sku'] = products[i].variant_sku;
                            variantsObj['active'] = products[i].active;
                            variantsObj['stock'] = products[i].stock;
                            variantsObj['shipping'] = products[i].shipping;
                            variantsObj['mrp'] = products[i].mrp;
                            variantsObj['price'] = products[i].price;
                            // variantsObj['sort'] = 1;
                            variants.push(variantsObj);
                            variantsObj = {};
                            productObj['variants'] = variants;
                            i++;
                        }
                        payload.push(productObj);
                        console.log(payload);
                        let product = {};
                        if (productObj.name) product.name = productObj.name;
                        if (productObj.sku) product.sku = productObj.sku;
                        if (productObj.brand) product.brand = productObj.brand;
                        if (productObj.metaTitle) product.metaTitle = productObj.metaTitle;
                        if (productObj.metaTitle) product.metaDescription = productObj.metaDescription;
                        if (productObj.metaKeywords) product.metaKeywords = productObj.metaKeywords;
                        if (productObj.uid) product.uid = productObj.uid;
                        if (productObj.vendor_id) product.vendor_id = productObj.vendor_id;
                        if (productObj.vendor_email) product.vendor_email = productObj.vendor_email;
                        if (productObj.active === 'FALSE') product.active = false;
                       await Product.updateOne({_id: productObj._id}, {$set: product});
                        const prod = Product.findById(productObj._id);
                        for (let j = 0; j < productObj.imageUrls.length; j++) { // Main image updation
                            let image = {};
                            if (productObj.imageUrls[i]) {
                                var pattern = /^((http|https|ftp):\/\/)/;
                                let isUrl = (pattern.test(productObj.imageUrls[j]));
                                if (isUrl) {
                                    wheatherUpdateImg = true;
                                    image['imgUrls.' + j] = productObj.imageUrls[j];
                                }
                            }
                         await   Product.updateOne({_id: productObj._id}, {$set: image})
                        }
                        for (let k = 0; k < productObj.categories.length; k++) { // Category updation
                            let category = {};
                            if (productObj.categories[k]) {
                                category['categories.' + k] = productObj.categories[k]
                            }
                           await Product.updateOne({_id: productObj._id}, {$set: category})
                        }

                        const prod1 =  Product.findById(productObj._id);
                        if (wheatherUpdateImg) {
                            try {
                                prod1.img =  prod1.imgUrl;
                                prod1.save()
                            } catch (e) { }
                        }
                        if (productObj.sizechartUrl && productObj.sizechartUrl.length > 0) { // If image has to come from a URL (Not user upload) // If the ckeckbox at admin panel clicked which says save images from URL to file system of server
                            try {
                                prod1.sizechart = productObj.sizechartUrl;
                                prod1.save()
                            } catch (e) {
                                console.log('generate image err.....', e.toString());
                            }
                        }
                        // console.log(productObj.variants.length);

                            for (let k = 0; k < productObj.variants.length; k++) { // Variants
                            let variant = {}; // Never add anything extra here. It will erase that field (e.g. features)
                            let _id = 'variants.' + k + '._id';
                            let size = 'variants.' + k + '.size';
                            let sku = 'variants.' + k + '.sku';
                            let stock = 'variants.' + k + '.stock';
                            let mrp = 'variants.' + k + '.mrp';
                            let price = 'variants.' + k + '.price';
                            let sort = 'variants.' + k + '.sort';
                            if (!productObj.variants[k].price) // Everything bellow will be skipped if no price found. Only required for Import stock
                                continue;
                            if (productObj.variants[k].size) {
                                variant[size] = productObj.variants[k].size
                            }
                            if (productObj.variants[k].sku) {
                                variant[sku] = productObj.variants[k].sku
                            }
                            if (productObj.variants[k].stock) {
                                variant[stock] = productObj.variants[k].stock
                            }
                            if (productObj.variants[k].sort) {
                                variant[sort] = productObj.variants[k].sort
                            }
                            if (productObj.variants[k].mrp) {
                                variant[mrp] = productObj.variants[k].mrp
                            }
                            if (productObj.variants[k].price) {
                                variant[price] = productObj.variants[k].price;
                                if (prod.variants && prod.variants[k] && prod.variants[k]._id) {
                                } else {
                                    variant[_id] = new ObjectId()
                                }
                            }
                           await  Product.updateOne({ _id: productObj._id }, { $set: variant })
                        }
                        for (let k = 0; k < productObj.features.length; k++) {

                            let feature = {}; // Never add anything extra here. It will erase that field (e.g. features)
                            let fk = 'features.' + k + '.key';
                            let fv = 'features.' + k + '.val';
                            if (!productObj.features[k]) // It will not add feature, if no key found
                                continue;
                            if (productObj.features[k].key) {
                                feature[fk] = productObj.features[k].key;
                            }
                            if (productObj.features[k].val) {
                                feature[fv] = productObj.features[k].val;
                            }
                            await Product.updateOne({ _id: productObj._id }, { $set:feature })
                        }



                        //
                        //             // Create a Note
                        //             const product = new Product(productObj);
                        //
                        //             // Save Note in the database
                        //             product.save()
                        //                 .then(data => {
                        //                     // console.log(JSON.stringify(data));
                        //                 }).catch(err => {
                        //                 res.status(500).send({
                        //                     message: err.message || "Some error occurred while creating the Note."
                        //                 });
                        success.push(productObj._id);
                        //             });
                        productObj = {};
                        }catch (e) {
                            errors.push(productObj._id);
                            console.log('err', e);
                            err = new Error(e);
                            csvStream.end(); // <<< stop parser
                        }
                    }
                    // console.log(JSON.stringify(payload));
                });
            // stream.pipe(csvStream);

            // stream.pipe(csvParser()).on('data', (data) => {
            //     for(var i = 0;i<products.length ;i++){
            //
            //     }
            //     products.push(data)
            // })
            //     .on('end', () => {
            //         // console.log(products);
            //         var sku_found = null;

            //         // console.log(JSON.stringify(payload));
            //     });
            return res.end();

        });
    });
};


// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Note.findByIdAndRemove(req.params.noteId)
        .then(note => {
            if (!note) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            res.send({message: "Note deleted successfully!"});
        }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.noteId
        });
    });
};