const Product = require('../models/product.model.js');
var fs = require('fs');
const csv = require('fast-csv');
const sorter = require('apparel-sorter');

// Create and Save a new Note
function arrayConverter(stack, option) {
    var convertedArray = [];
    var obj = {};
    var i = 0;
    if (option === 'categories') {
        for (; i < stack.length; i++) {
            convertedArray.push(stack[i]._id)
        }
    } else if (option === 'images') {

        for (; i < stack.length; i++) {
            if (i === 0) {
                obj['featured_image'] = stack[i];
            } else {
                convertedArray.push(stack[i])
            }
        }
    } else if (option === 'features') {

        for (; i < stack.length; i++) {
            convertedArray.push([stack[i].key + ':' + stack[i].val])
        }
    }
    obj['string'] = convertedArray.toString();
    return obj
}

function positioning(variants) {
    var sizes = [];
    var i = 0;
    for (; i < variants.length; i++) {
        sizes.push(variants[i].size.toString())
    }
    let filteredValues = sorter.sort(sizes);
    for (i = 0; i < variants.length; i++) {
        variants[i].position = filteredValues.indexOf(variants[i].size.toString());
    }
    return variants;
}

exports.create = (req, res) => {
    // Validate request
    if (!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Create a Product
    const product = new Product({
        title: req.body.title || "Untitled Note",
        content: req.body.content
    });

    // Save Note in the database
    product.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Product.find().skip(parseInt(req.query.skip) || 0).limit(parseInt(req.query.limit) || 30)
        .then(products => {
            var csvStream = csv.createWriteStream({headers: true}),
                writableStream = fs.createWriteStream("products.csv");

            writableStream.on("finish", function () {
                console.log("DONE!");
            });

            csvStream.pipe(writableStream);
            const filteredValues = sorter.sort(['45', '46', '32', '22', '32', '22']);
            // const sortAlphaNum = (a, b) => a.localeCompare(b, 'en', { numeric: true });
            // console.log([6, 'A10', 'A11', 'A12', 'A2', 'A3', 'A4', 'B10', 'B2', 'F1', 'F12', 'F3'].sort());
            console.log(filteredValues);
            for (var i = 0; i < products.length; i++) {
                var productCategories = arrayConverter(products[i].categories, 'categories');
                var productImages = arrayConverter(products[i].imgUrls, 'images');
                var productFeatures = arrayConverter(products[i].features, 'features');
                var variants = positioning(products[i].variants);

                for (var j = 0; j < variants.length; j++) {
                    var productObj = {};
                    productObj['product_id'] = products[i]._id;
                    productObj['name'] = products[i].name;
                    productObj['sku'] = products[i].sku;
                    productObj['categories'] = productCategories.string;
                    productObj['featured_image'] = productImages.featured_image;
                    productObj['imageUrls'] = productImages.string;
                    productObj['features'] = productFeatures.string;
                    productObj['vendor_name'] = products[i].vendor_name;
                    productObj['vendor_email'] = products[i].vendor_email;
                    productObj['description'] = products[i].description;
                    productObj['metaTitle'] = products[i].metaTitle;
                    productObj['metaDescription'] = products[i].metaDescription;
                    productObj['metaKeywords'] = products[i].metaKeywords;
                    productObj['brandName'] = products[i].brandName;
                    productObj['sizechart'] = products[i].sizechart;
                    productObj['new'] = products[i].new;
                    productObj['sale'] = products[i].sale;
                    productObj['hot'] = products[i].hot;
                    productObj['approved'] = products[i].approved;
                    productObj['active'] = products[i].active;
                    productObj['popularity'] = products[i].popularity;
                    productObj['featured'] = products[i].featured;
                    productObj['enableZips'] = products[i].enableZips;
                    productObj['featured'] = products[i].featured;
                    productObj['size'] = variants[j].size;
                    productObj['variant_sku'] = variants[j].sku;
                    productObj['variant_id'] = variants[j]._id;
                    productObj['active'] = variants[j].active;
                    productObj['stock'] = variants[j].stock;
                    productObj['shipping'] = variants[j].shipping;
                    productObj['mrp'] = variants[j].mrp;
                    productObj['price'] = variants[j].price;
                    productObj['position'] = variants[j].position;
                    csvStream.write(productObj);
                    productObj = {};
                }
            }
            csvStream.end();
            res.send(products);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Note.findById(req.params.noteId)
        .then(products => {
            if (!products) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            res.send(note);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.noteId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    Note.findByIdAndUpdate(req.params.noteId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, {new: true})
        .then(note => {
            if (!note) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            res.send(note);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.noteId
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Note.findByIdAndRemove(req.params.noteId)
        .then(note => {
            if (!note) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            res.send({message: "Note deleted successfully!"});
        }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.noteId
        });
    });
};
