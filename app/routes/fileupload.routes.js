module.exports = (app) => {
    const fileuploads = require('../controllers/fileupload.controller.js');
    // Create a new Note
    app.post('/fileupload', fileuploads.create);
    // Delete a Note with noteId
    app.delete('/fileupload/:noteId', fileuploads.delete);
}