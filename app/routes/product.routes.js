module.exports = (app) => {
    const products = require('../controllers/product.controller.js');

    // Create a new Note
    app.post('/products', products.create);

    // Retrieve all Notes
    app.get('/products', products.findAll);

    // Retrieve a single Note with noteId
    app.get('/products/:productId', products.findOne);

    // Update a Note with noteId
    app.put('/products/:productId', products.update);

    // Delete a Note with noteId
    app.delete('/products/:productId', products.delete);
}