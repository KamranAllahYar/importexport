const mongoose = require('mongoose');
let ObjectId = mongoose.Schema.ObjectId;
//db.products.createIndex( { name: "text", description: "text",brand:'text',keyFeatures:'text',sku:'text',fkBrand:'texom './../base';

var schemaOptions = {
  toObject: { getters: true },
  toJSON: { getters: true },
  versionKey: false,
  timestamps: true
};

let ImgSchema = new mongoose.Schema({
  original: String,
  small: String,
  medium: String,
  large: String
})

let variantsSchema = new mongoose.Schema({
  img: [], imgUrls: { type: Array, default: [] }, price: { type: Number, default: 0 }, mrp: { type: Number, default: 0 }, offer: { type: Number, default: 0 }, shipping: { type: Number, default: 0 },
  weight: String, size: String, color: String, trackInventory: { type: Boolean, default: false }, stock: { type: Number, default: 100000 }, unit: { type: String, default: 'None' }, sku: String,
  barcode: String, sameImages: Boolean, active: { type: Boolean, default: true }, enableUnitPrice: { type: Boolean, default: false },
  saleFromDate: { type: Date, default: Date.now },
  saleToDate: { type: Date, default: () => Date.now() + 1 * 365 * 24 * 60 * 60 * 1000 }
});

// MRP Setter
variantsSchema.path('mrp').set(function (num) {
  return (num * 100 / 100).toFixed(2);
});

// Price Setter
variantsSchema.path('price').set(function (num) {
  return (num * 100 / 100).toFixed(2);
});

let ProductSchema = new mongoose.Schema({
  sku: String,
  color: { name: String, val: String },
  group: String,
  promoText: String,
  name: String,
  nameLower: String,
  slug: String,
  img: { type: [ImgSchema], default: [] },
  imgUrls: { type: Array, default: [] },
  enableZips: { type: Boolean, default: false },
  zips: [{ type: Number }],
  category: String,
  parentCategory: { type: ObjectId, ref: 'Category' },
  cat: {},
  categories: Array,
  categoryQ: String,
  status: String,
  brand: { type: ObjectId, ref: 'Brand' },
  brandName: String,
  brandSlug: String,
  description: String,
  meta: String,
  metaTitle: String,
  metaDescription: String,
  metaKeywords: String,
  variants: [variantsSchema],
  features: [{ key: String, val: String }],
  featured: { type: Boolean, default: false },
  position: { type: Number, default: 0 },
  keyFeatures: Array,
  popularity: { type: Number, default: 0 },
  uid: String, // can not use ObjectId for join(as of Category) as we store email here
  vendor_id: { type: ObjectId, ref: 'User' },
  vendor_name: String, // Store vendor name here
  vendor_email: String, // can not use ObjectId for join(as of Category) as we store vendor email here
  updated: { type: Date, default: Date.now },
  active: { type: Boolean, default: true },
  approved: { type: Boolean, default: true },
  hot: { type: Boolean, default: false },
  sale: { type: Boolean, default: false },
  offer: Object,
  q: String,
  q1: String,
  new: { type: Boolean, default: false },
  related: [{ type: ObjectId, ref: 'Product' }],
  sizechart: String,
  sizechartUrl: Object,
  validFromDate: { type: Date, default: Date.now() },
  validToDate: { type: Date, default: () => Date.now() + 20 * 365 * 24 * 60 * 60 * 1000 },
  imageUploaded: Date
}, schemaOptions);


ProductSchema.pre('save', function (next) {
  this.categoryQ = this.categories ? this.categories + ' ' : ''
  this.q = this.sku ? this.sku + ' ' : ''
  this.q = this.name ? this.name + ' ' : ''
  this.q += this.description ? this.description + ' ' : ''
  this.q += this.category ? this.category + ' ' : ''
  this.q += this.status ? this.status + ' ' : ''
  this.q += ' '
  next();
});
module.exports = mongoose.model('Product', ProductSchema);